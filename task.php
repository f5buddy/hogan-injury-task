<?php
/**
 * Instructions:
 * Write a solution in PHP:
 *
 * How your implementation works:
 * Your function will take two arguments, ($prevArray, $currArray), flattens the objects inside of prevArray and currArray to 1 level of
 * depth, and return an HTML Table in string form of the values.  The HTML table you return has a column header which is a superset of all keys in 
 * all the objects in the currArray.  Any values that have changed from the prevArray to the currArray (ie field value changed or is a 
 * new key altogether) should be bolded. In the case that the value has been removed altogether from the prevArray to the currArravy, 
 * you will write out the key in bold DELETED.
 * 
 * Rules:
 * 1. The arrays are arbitrarily deep (see common questions for explanation of arbitrarily deep).
 * 2. The currArray could have more or potentially even be in a different index order.  You cannot depend solely on array index for  
 * comparison.  However, you can assume that each object in the arrays will have an "_id" parameter.  Unless the currArray has no  
 * object with the matching "_id" parameter (for example if the whole row has changed).
 * 3. Do not create global scope.  We have a test runner that will iterate on your function and run many fixtures through it.  If you 
 * create global scope for 1 individual diff between prevArray to currArray you could cause other tests to fail.  
 *
 * Common Questions:
 * 1. Can I use outside packages to solve (e.g. Composer)?  Yes.  You can use any packages you want to solve the solution.  
 * 2. Can I use google or outside resources (e.g. StackOverflow, GitHub)?  Yes.  Act as you would in your day job.
 * 3. What does arbitrarily deep mean? The $prevArray or $currArray can have objects inside of objects at different levels of depth. 
 *    You will not know how many levels of depth the objects could have, meaning your code must handle any kind of object.  Your 
 *    solution  must account for this.  Do not assume the examples below are the only fixtures we will use to test your code. 
 * 
 * @param $prevArray is a JSON string containing an array of objects
 * @param $currArray is a JSON string containing an array of objects
 * @return a string with HTML markup in it, should return null if error occurs.
 */

$prevArray = '[{"_id":1,"someKey":"RINGING","meta":{"subKey1":1234,"subKey2":52}}]';
$currArray = '[{"_id":1,"someKey":"HANGUP","meta":{"subKey1":1234}},{"_id":2,"someKey":"RINGING","meta":{"subKey1":5678,"subKey2":207,"subKey3":52}}]';

function check_id($array,$value)
{
    $array_count=count($array);
    for ($i=0; $i < $array_count; $i++) { 
	    if($array[$i]['_id']==$value)
	    {
		     return 1;
	    }
	    else
	    {
		    return 0;
	    }
    }
}

function check_someKey($array,$value)
{
    $array_count=count($array);
    for ($i=0; $i < $array_count; $i++) { 
	    if($array[$i]['someKey']==$value)
	    {
    		 return true;
    	}
    	else
    	{
    		return false;
    	}
    }
}

//***************function start from here
function arrayDiffToHtmlTable( $prevArray, $currArray) {
    //IMPLEMENT
$prevArray=json_decode($prevArray, true);
$currArray=json_decode($currArray, true);

$prevArray_count=count($prevArray);
$currArray_count=count($currArray);
?>


<table width="100%" border="1" style="border-collapse: collapse; ">

<tr style="font-weight: bold;">
<td>_id </td>
<td>someKey </td>
<?php for ($i=0; $i<$currArray_count ; $i++) 
      {
	      for($j=1;$j<=count($currArray[$i]['meta']);$j++)
	      {
			echo '<td>meta_subKey'.$j.'</td>';
		  }
	  }
?>
</tr>
<?php
      for ($i=0; $i<$currArray_count ; $i++) 
      {
?>	
<tr>
<td><?php if(check_id($prevArray,$currArray[$i]['_id']))
    {
     echo $currArray[$i]['_id'];

	}
	else{
		  echo '<strong>'.$currArray[$i]['_id'].'</strong>';
		} ?>
</td>

<td><?php 
 if(check_someKey($prevArray,$currArray[$i]['someKey']))
    {
     echo $currArray[$i]['someKey'];

	}
	else{
		  echo '<strong>'.$currArray[$i]['someKey'].'</strong>';
		}
?>
 </td>

<?php 
for($j=1;$j<=count($currArray[$i]['meta']);$j++)
{
?>
<td><?php echo $currArray[$i]['meta']['subKey'.$j]?></td>
<?php 
}
?> 
</tr>
<?php
}
?>
<table>
<?php }
    echo arrayDiffToHtmlTable( $prevArray, $currArray);
?>